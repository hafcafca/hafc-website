import {Component, OnInit} from '@angular/core';
import {CommentsService} from './services/comments.service';
import {PostsService} from './services/posts.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(private postsService: PostsService, private commentService: CommentsService) {}

  posts;
  comments;

  ngOnInit() {

  }

}
