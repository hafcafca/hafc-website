import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VolgendewedstrijdComponent } from './volgendewedstrijd.component';

describe('VolgendewedstrijdComponent', () => {
  let component: VolgendewedstrijdComponent;
  let fixture: ComponentFixture<VolgendewedstrijdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VolgendewedstrijdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VolgendewedstrijdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
