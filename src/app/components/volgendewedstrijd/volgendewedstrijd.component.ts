import { Component, OnInit } from '@angular/core';
import {VoetbalapiService} from '../../services/voetbalapi.service';

@Component({
  selector: 'app-volgendewedstrijd',
  templateUrl: './volgendewedstrijd.component.html',
  styleUrls: ['./volgendewedstrijd.component.scss']
})
export class VolgendewedstrijdComponent implements OnInit {

  constructor(private voetbalService: VoetbalapiService) { }

  thuisClub;
  uitClub;
  speelMoment;
  stadion;

  ngOnInit() {
    this.loadVolgendeWedstrijd();
  }

  loadVolgendeWedstrijd() {
    this.voetbalService.getProgramma().subscribe(
      (programma) => {
        this.thuisClub = programma[0].thuisClub;
        this.uitClub = programma[0].uitClub;
        if (this.thuisClub === 'Heracles Almelo') {
          this.thuisClub = 'Heracles';
        }
        if (this.uitClub === 'Heracles Almelo') {
          this.uitClub = 'Heracles';
        }
        this.speelMoment = programma[0].speelMoment;
        this.stadion = programma[0].stadion;
      }
    )
  }

}
