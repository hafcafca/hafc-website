import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators } from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  form: FormGroup;
  searchString;

  constructor(private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      searchString: [null, [Validators.required]]
    });
  }

  onSubmit() {
    this.searchString = this.form.value.searchString;
    console.log(this.searchString);
    this.router.navigate(['/zoeken/' + this.searchString])
  }

}
