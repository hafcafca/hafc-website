import { Component, OnInit } from '@angular/core';
import {InstagramService} from '../../services/instagram.service';

@Component({
  selector: 'app-instagram',
  templateUrl: './instagram.component.html',
  styleUrls: ['./instagram.component.scss']
})
export class InstagramComponent implements OnInit {

  constructor(private instagramService: InstagramService) { }

  instagramItems;

  ngOnInit() {
    this.loadInstagramPosts();
  }

  loadInstagramPosts() {
    this.instagramService.getInstagramPosts().subscribe(
      (instagramItems) => {
        this.instagramItems = instagramItems.data;
      }
    )
  }

}
