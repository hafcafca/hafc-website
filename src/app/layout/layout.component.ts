import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

  showNav = false;

  constructor() { }

  ngOnInit() {
  }

  toggleNav() {
    if (this.showNav === false) {
      this.showNav = true;
    } else {
      this.showNav = false;
    }
  }

  closeMenu() {
    this.showNav = false;
  }

}
