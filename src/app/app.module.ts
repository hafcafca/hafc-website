import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LayoutComponent } from './layout/layout.component';
import {HttpClientModule} from '@angular/common/http';
import {CommentsService} from './services/comments.service';
import {PostsService} from './services/posts.service';
import { HomeComponent } from './pages/home/home.component';
import { HeadlineComponent } from './pages/home/headline/headline.component';
import { NewsitemsComponent } from './pages/home/newsitems/newsitems.component';
import { FooterComponent } from './layout/footer/footer.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { NguCarouselModule } from '@ngu/carousel';
import { RouterModule, Routes } from '@angular/router';
import {VoetbalapiService} from './services/voetbalapi.service';
import { NewsitemComponent } from './pages/newsitem/newsitem.component';
import {InstagramService} from './services/instagram.service';
import { VolgendewedstrijdComponent } from './components/volgendewedstrijd/volgendewedstrijd.component';
import { InstagramComponent } from './components/instagram/instagram.component';
import { LoaderComponent } from './components/loader/loader.component';
import { WedstrijdenComponent } from './pages/eredivisie/wedstrijden/wedstrijden.component';
import { ZoekenComponent } from './pages/zoeken/zoeken.component';
import { SearchComponent } from './components/search/search.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NewsitemsOverzichtComponent } from './pages/newsitemsoverzicht/newsitemsoverzicht.component';
import {Modal, ModalModule} from "ngx-modal";
import { NotfoundComponent } from './pages/notfound/notfound.component';
import {AdsenseModule} from 'ng2-adsense';


const routes: Routes = [
  { path: 'nieuws',
    component: NewsitemsOverzichtComponent
  },
  { path: 'eredivisie/wedstrijden',
    component: WedstrijdenComponent
  },
  {
    path: 'nieuws/:id',
    component:  NewsitemComponent
  },
  {
    path: 'zoeken/:searchString',
    component:  ZoekenComponent
  },
  { path: '',
    component: HomeComponent
  },
  { path: '404',
    component: NotfoundComponent
  },
  {
    path: '**', redirectTo: '/404'
  }
]

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    HomeComponent,
    HeadlineComponent,
    NewsitemsComponent,
    FooterComponent,
    NewsitemComponent,
    NewsitemsOverzichtComponent,
    VolgendewedstrijdComponent,
    InstagramComponent,
    LoaderComponent,
    WedstrijdenComponent,
    ZoekenComponent,
    SearchComponent,
    NotfoundComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({
      appId: 'universal'
    }),
    AdsenseModule.forRoot({
      adClient: 'ca-pub-4265409979767213',
      adSlot: 9711506203,
    }),
    HttpClientModule,
    NguCarouselModule,
    ModalModule,
    InfiniteScrollModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [PostsService, CommentsService, VoetbalapiService, InstagramService, Modal],
  bootstrap: [AppComponent]
})
export class AppModule { }
