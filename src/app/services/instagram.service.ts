import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders } from '@angular/common/http'
import {Observable} from 'rxjs/Observable';

@Injectable()

export class InstagramService {
  constructor (private http: HttpClient) {}

  options = {
    headers: {

    }
  }

  public getInstagramPosts():Observable<any> {
    return this.http.get('https://api.instagram.com/v1/users/self/media/recent?access_token=2112523757.1677ed0.e60d2dc4389847eabb3464ddaca4f85c&count=8', this.options)
  }

}
