import {Inject, Injectable, Optional} from '@angular/core';
import {HttpClient, HttpHeaders } from '@angular/common/http'
import {Observable} from 'rxjs/Observable';
import {APP_BASE_HREF} from '@angular/common';

@Injectable()

export class PostsService {
  constructor (private http: HttpClient, @Optional() @Inject(APP_BASE_HREF) origin: string) {}

  options = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  public getPosts(page: number = 1, per_page: number = 8, searchString: string = undefined):Observable<any> {
    if (searchString === undefined) {
      return this.http.get('https://hafc.nl/wp-json/wp/v2/posts?page=' + page + '&per_page=' + per_page, this.options)
    } else {
      return this.http.get('https://hafc.nl/wp-json/wp/v2/posts?page='+ page + '&search=' + searchString + '&per_page=' + per_page , this.options)
    }
  }

  public getPost(id):Observable<any> {
    return this.http.get('https://hafc.nl/wp-json/wp/v2/posts/'+ id, this.options);
  }

}
