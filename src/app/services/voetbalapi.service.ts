import {HttpClient, HttpHeaders} from '@angular/common/http'
import {Injectable} from '@angular/core';

@Injectable()
export class VoetbalapiService {
  constructor(private http: HttpClient) {}
  options = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Cache-control': 'no-cache'
    })
  }
  getEredivisieStand():any {
    return this.http.get('http://voetbalapi.nl/competities/eredivisie/stand', this.options);
  }
  getProgramma() {
    return this.http.get('http://voetbalapi.nl/clubs/heracles/geplandewedstrijden', this.options);
  }
  getResultaten() {
    return this.http.get('http://voetbalapi.nl/clubs/heracles/gespeeldewedstrijden', this.options);
  }
}
