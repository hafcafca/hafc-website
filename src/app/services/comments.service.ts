import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Comment} from '../models/comment.model';

@Injectable()

export class CommentsService {
  constructor(private http: HttpClient) {}

  public getComments(newsId: number, page: number = 1):Observable<any> {
    return this.http.get('https://hafc.nl/wp-json/wp/v2/comments?post=' + newsId + '&page=' + page);
  }

  public postComment(comment: Comment) {
    return this.http.post('https://www.hafc.nl/wp-json/wp/v2/comments?post=' + comment.post, comment)
  }

}
