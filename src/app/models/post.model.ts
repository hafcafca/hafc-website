export interface PostModel {
  postId: number,
  postDate: Date,
  postTitle: string,
  postFeaturedImage: string,
  postAuthor: string,
  postContent: string
}
