export class Comment {

  public author_email: string;
  public author_name: string;
  public author_url: string;
  public content: string;
  public date: Date;
  public date_gmt: Date;
  public karma: string;
  public parent: number;
  public post: number;
  public type: string;

  constructor(author_email: string, author_name: string, author_url: string, content: string, date: Date, date_gmt: Date, karma: string, parent: number, post: number, type: string) {
    this.author_email = author_email;
    this.author_name = author_name;
    this.author_url = author_url;
    this.content = content;
    this.date = date;
    this.date_gmt = date_gmt;
    this.karma = karma;
    this.parent = parent;
    this.post = post;
    this.type = type;
  }

}
