import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {PostsService} from '../../services/posts.service';
import {ActivatedRoute} from '@angular/router';
import {CommentsService} from '../../services/comments.service';
import {Meta, Title} from '@angular/platform-browser';
import {User} from '../../models/user.model';
import {Comment} from '../../models/comment.model';
import {Modal} from 'ngx-modal'


@Component({
  selector: 'app-newsitem',
  templateUrl: './newsitem.component.html',
  styleUrls: ['./newsitem.component.scss']
})
export class NewsitemComponent implements OnInit {


  constructor(private postsService: PostsService,
              private commentsService: CommentsService,
              private route: ActivatedRoute,
              private titleService: Title,
              private meta: Meta,
              private modal: Modal) {}

  post;
  comments;
  page = 1;
  showLoader = false;
  comment: Comment;
  user: User = {
    name: '',
    email: ''
  };
  content: string;

  @ViewChild('commentModal') commentModal: Modal;
  @ViewChild("commentSection") commentsEl: ElementRef;

  ngOnInit() {
    this.route.params.subscribe(
      (params) => {
        this.loadNieuwsBericht(params.id);
        this.loadComments(params.id);
      }
    )
  }

  onScroll() {
    this.page++;
    this.showLoader = true;
    this.route.params.subscribe(
      (params) => {
        this.commentsService.getComments(params.id, this.page).subscribe(
          (comments) => {
            this.comments = this.comments.concat(comments);
            this.showLoader = false;
          }
        )
      }
    )
  }

  loadNieuwsBericht(id) {
    this.postsService.getPost(id).subscribe(
      (post) => {
        this.post = post;
        this.titleService.setTitle(this.post.title.rendered + ' - HAFC.nl');
        this.meta.addTag({
          name: 'description',
          content: this.post.title.rendered
        });
      }
    )
  }

  goToComments() {
    this.commentsEl.nativeElement.scrollIntoView({
      behavior: "smooth",
      block: "start"
    });
  }

  addComment() {
    this.route.params.subscribe(
      (params) => {
        this.comment = new Comment(this.user.email, this.user.name, '', this.content, new Date(), new Date(), '1', 0, params.id, '');
        this.commentsService.postComment(this.comment).subscribe(
          (response: Response) => {
            this.loadComments(params.id);
            this.commentModal.close();
          },
          (error) => {
            console.log('Error ' + error);
          }
        );
      },
      (error) => {
        console.log('Error posting comment')
      }
    );
  }

  loadComments(newsId) {
    this.showLoader = true;
    this.commentsService.getComments(newsId).subscribe(
      (comments) => {
        this.comments = comments;
        this.showLoader = false;
      }
    )
  }

}
