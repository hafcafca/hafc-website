import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsitemsOverzichtComponent} from './newsitemsoverzicht.component'

describe('NewsitemsComponent', () => {
  let component: NewsitemsOverzichtComponent;
  let fixture: ComponentFixture<NewsitemsOverzichtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsitemsOverzichtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsitemsOverzichtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
