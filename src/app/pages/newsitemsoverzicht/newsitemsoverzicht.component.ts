import { Component, OnInit } from '@angular/core';
import {PostsService} from '../../services/posts.service';
import {Meta, Title} from '@angular/platform-browser';

@Component({
  selector: 'app-newsitemsoverzicht',
  templateUrl: './newsitemsoverzicht.component.html',
  styleUrls: ['./newsitemsoverzicht.component.scss']
})
export class NewsitemsOverzichtComponent implements OnInit {

  constructor(private postsService: PostsService,
              private titleService: Title,
              private meta: Meta) { }

  page = 1;
  posts;
  showLoader;

  ngOnInit() {
    this.postsService.getPosts(this.page, 8).subscribe(
      (posts) => {
        this.posts = posts;
      }
    )
    this.setTags();
  }

  setTags() {
    this.titleService.setTitle('Nieuwsoverzicht van Heracles Almelo');
    this.meta.addTag({
      name: 'description',
      content: 'Bekijk hier het nieuw van Heracles Almelo'
    });
  }

  onScroll() {
    this.page++;
    this.showLoader = true;
    this.postsService.getPosts(this.page, 8).subscribe(
      (posts) => {
        this.posts = this.posts.concat(posts);
        this.showLoader = false;
      }
    )
  }

}
