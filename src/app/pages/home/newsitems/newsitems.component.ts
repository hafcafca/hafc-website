import {Component, Input, OnInit} from '@angular/core';
import {PostsService} from '../../../services/posts.service';
import { AdsenseModule } from 'ng2-adsense';

@Component({
  selector: 'app-newsitems',
  templateUrl: './newsitems.component.html',
  styleUrls: ['./newsitems.component.scss']
})
export class NewsitemsComponent implements OnInit {

  constructor(private postsService: PostsService) { }

  @Input('posts') posts;
  count = 2;

  ngOnInit() {
    // console.log(this.posts);
  }

  onScroll () {

  }

  loadMore() {
    this.count++;
    console.log(this.posts);
    this.postsService.getPosts(this.count, 4).subscribe(
      (posts) => {
        this.posts = this.posts.concat(posts);
      }
    )
  }

}
