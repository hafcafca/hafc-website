import { Component, OnInit } from '@angular/core';
import {PostsService} from '../../services/posts.service'
import {VoetbalapiService} from '../../services/voetbalapi.service';
import {InstagramService} from '../../services/instagram.service';
import {Title, Meta} from '@angular/platform-browser'
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private postsService: PostsService,
              private voetbalService: VoetbalapiService,
              private instagramService: InstagramService,
              private titleService: Title,
              private meta: Meta,
              private router: Router) { }

  stand;
  posts;
  headlines;
  programma;

  ngOnInit() {
    this.loadNieuws();
    this.loadEredivisieStand();
    this.loadProgramma();
    this.titleService.setTitle('HAFC.nl');
    this.meta.addTag({
      name: 'description',
      content: 'HAFC.nl - Het supportersplatform van Heracles Almelo'
    });
  }

  loadNieuws() {
    this.postsService.getPosts().subscribe(
      (posts) => {
        this.posts = posts.slice(4,9);
        this.headlines = posts.slice(0, 3);
      }
    )
  }

  loadEredivisieStand() {
    this.voetbalService.getEredivisieStand().subscribe(
      (stand) => {
        this.stand = stand.stand;
      }
    )
  }

  loadProgramma() {
    this.voetbalService.getProgramma().subscribe(
      (programma) => {
        this.programma = programma;
        this.programma = this.programma.slice(0, 5);
      }
    )
  }

}
