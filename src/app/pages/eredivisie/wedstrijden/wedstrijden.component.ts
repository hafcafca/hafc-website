import { Component, OnInit } from '@angular/core';
import {VoetbalapiService} from '../../../services/voetbalapi.service';
import {Meta, Title} from '@angular/platform-browser';

@Component({
  selector: 'app-wedstrijden',
  templateUrl: './wedstrijden.component.html',
  styleUrls: ['./wedstrijden.component.scss']
})
export class WedstrijdenComponent implements OnInit {

  constructor(private voetbalService: VoetbalapiService,
              private titleService: Title,
              private meta: Meta) { }

  gespeeldeWedstrijden;
  programma;
  stand;

  ngOnInit() {
    this.loadEredivisieStand();
    this.loadProgramma();
    this.loadResultaten();
    this.setTags();
  }

  setTags() {
    this.titleService.setTitle('Programma en uitslagen van Heracles');
    this.meta.addTag({
      name: 'description',
      content: 'Bekijk hier het programma en de uitslagen van Heracles in de Eredivisie'
    });
  }

  loadProgramma() {
    this.voetbalService.getProgramma().subscribe(
      (programma) => {
        this.programma = programma;
      }
    )
  }

  loadResultaten() {
    this.voetbalService.getResultaten().subscribe(
      (gespeeldeWedstrijden) => {
        this.gespeeldeWedstrijden = gespeeldeWedstrijden;
      }
    )
  }

  loadEredivisieStand() {
    this.voetbalService.getEredivisieStand().subscribe(
      (stand) => {
        this.stand = stand.stand;
      }
    )
  }

}
