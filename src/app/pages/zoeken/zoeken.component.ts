import { Component, OnInit } from '@angular/core';
import {PostsService} from '../../services/posts.service';
import {ActivatedRoute} from '@angular/router';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';


@Component({
  selector: 'app-zoeken',
  templateUrl: './zoeken.component.html',
  styleUrls: ['./zoeken.component.scss']
})
export class ZoekenComponent implements OnInit {

  constructor(private postsService: PostsService, private route: ActivatedRoute) { }

  searchString;
  posts;
  page = 1;
  showLoader;

  ngOnInit() {
    this.route.params.subscribe(
      (params) => {
        this.searchString = params.searchString;
        this.postsService.getPosts(this.page, 8, this.searchString).subscribe(
          (posts) => {
            this.posts = posts;
          }
        )
      }
    )
  }

  onScroll() {
    this.page++;
    this.showLoader = true;
    this.route.params.subscribe(
      (params) => {
        this.postsService.getPosts(this.page, 8, this.searchString).subscribe(
          (posts) => {
            this.posts = this.posts.concat(posts);
            this.showLoader = false;
          }
        )
      }
    )
  }
}
